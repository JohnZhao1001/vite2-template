
export function clone(target, map = new WeakMap()) {
  /* 基础常量 */
const mapTag = "[object Map]";
const setTag = "[object Set]";
const arrayTag = "[object Array]";
const objectTag = "[object Object]";
const argsTag = "[object Arguments]";
const boolTag = "[object Boolean]";
const dateTag = "[object Date]";
const numberTag = "[object Number]";
const stringTag = "[object String]";
const symbolTag = "[object Symbol]";
const errorTag = "[object Error]";
const regexpTag = "[object RegExp]";
const funcTag = "[object Function]";
const deepTag = [mapTag, setTag, arrayTag, objectTag, argsTag];

function forEach(array, iteratee) {
  let index = -1;
  const length = array.length;
  while (++index < length) {
    iteratee(array[index], index);
  }
  return array;
}

function isObject(target) {
  const type = typeof target;
  return target !== null && (type === "object" || type === "function");
}

function getType(target) {
  console.log(target);
  return Object.prototype.toString.call(target);
}

function getInit(target) {
  const Ctor = target.constructor;
  return new Ctor();
}

function cloneSymbol(targe) {
  return Object(Symbol.prototype.valueOf.call(targe));
}

function cloneReg(targe) {
  const reFlags = /\w*$/;
  const result = new targe.constructor(targe.source, reFlags.exec(targe));
  result.lastIndex = targe.lastIndex;
  return result;
}

function cloneFunction(func) {
  const bodyReg = /(?<={)(.|\n)+(?=})/m;
  const paramReg = /(?<=\().+(?=\)\s+{)/;
  const funcString = func.toString();
  if (func.prototype) {
    const param = paramReg.exec(funcString);
    const body = bodyReg.exec(funcString);
    if (body) {
      if (param) {
        const paramArr = param[0].split(",");
        return new Function(...paramArr, body[0]);
      } else {
        return new Function(body[0]);
      }
    } else {
      return null;
    }
  } else {
    return eval(funcString);
  }
}

function cloneOtherType(targe, type) {
  const Ctor = targe.constructor;
  switch (type) {
    case boolTag:
    case numberTag:
    case stringTag:
    case errorTag:
    case dateTag:
      return new Ctor(targe);
    case regexpTag:
      return cloneReg(targe);
    case symbolTag:
      return cloneSymbol(targe);
    case funcTag:
      return cloneFunction(targe);
    default:
      return null;
  }
}

  // 克隆原始类型
  if (!isObject(target)) return target;
  // 初始化
  const type = getType(target);
  let cloneTarget;
  if (deepTag.includes(type)) {
    cloneTarget = getInit(target);
  } else {
    return cloneOtherType(target, type);
  }

  // 防止循环引用
  if (map.get(target)) return map.get(target);
  map.set(target, cloneTarget);

  // 克隆set
  if (type === setTag) {
    target.forEach(value => {
      cloneTarget.add(clone(value, map));
    });
    return cloneTarget;
  }

  // 克隆map
  if (type === mapTag) {
    target.forEach((value, key) => {
      cloneTarget.set(key, clone(value, map));
    });
    return cloneTarget;
  }

  // 克隆对象和数组
  const keys = type === arrayTag ? undefined : Reflect.ownKeys(target);
  forEach(keys || target, (value, key) => {
    if (keys) {
      key = value;
    }
    cloneTarget[key] = clone(target[key], map);
  });

  return cloneTarget;
}
export const getAnyKey = (obj, key: string | any, defaultValue): any => {
  if (!key) return defaultValue;
  const keyList = key.split(".");
  const evalStr = keyList.reduce((total, cur) => {
    return total + `?.${cur}`;
  }, `obj`);
  const val = eval(evalStr);
  return val ? val : defaultValue;
};
export type CodeTip = "<" |">" | "{"|"}"|"["|"]"|"("|")"
export const  isForMatCode = (list:Array<CodeTip>)=>{
  if(!list.length || (list.length%2  === 1)) return false;
  let catchIndex:Array<number>=[]
  const keyOfEnd = {
    "[":"]",
    "{":"}",
    "<":">",
    "(":")",
  }
  try {
    list.forEach((v,i)=>{
      if(!catchIndex.includes(i)){
        if(Reflect.ownKeys(keyOfEnd).includes(v)){
          const temp = list.slice(i);
          if(temp.length) {
          const index =  temp.findIndex(k => k===keyOfEnd[v])
          if(index === -1) {
            throw Error(`未正确闭合括号[${i+1}:${v}]`)
          } else {
            catchIndex.push(i)
            catchIndex.push(index+i)
          }
          } else {
            throw Error(`未正确闭合括号[${i+1}:${v}]`)
          }
        } else {
          throw Error(`未正确闭合括号[${i+1}:${v}]`)
        }
      }
    })
  } catch (error) {
    return false
  }
  return true
}
 // 将数字转换成金额显示
 export function toMoney(num) {
  if (num) {
    if (isNaN(num)) {
      alert('金额中含有不能识别的字符');
      return;
    }
    num = typeof num == 'string' ? parseFloat(num) : num // 判断是否是字符串如果是字符串转成数字
    num = num.toFixed(2); // 保留两位
    console.log(num)
    num = parseFloat(num); // 转成数字
    num = num.toLocaleString(); // 转成金额显示模式
    // 判断是否有小数
    if (num.indexOf('.') === -1) {
      num = '￥' + num + '.00';
    } else {
      console.log(num.split('.')[1].length)
      // num = num.split('.')[1].length < 2 ? '￥' + num + '0' : '￥' + num;
    }
    return num; // 返回的是字符串23,245.12保留2位小数
  } else {
    return num = null;
  }
}
